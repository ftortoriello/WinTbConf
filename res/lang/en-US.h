#define RES_LANG         LANG_ENGLISH
#define RES_SUBLANG      SUBLANG_ENGLISH_US

#define S_ERROR          "Error"
#define S_ERROR_GENERIC  "An error has occurred"
#define S_ERROR_MEM      "Could not allocate memory"

#define S_TB  "General Settings"

#define S_TB_TB                 "Taskbar"
#define S_TB_LOCK               "&Lock the taskbar"
#define S_TB_AUTOHIDE           "A&utomatically hide the taskbar"
#define S_TB_SMALLBUTTONS       "Us&e small buttons"
#define S_TB_BADGES             "Show badges on buttons"
#define S_TB_LOCATION           "Location on screen:"
#define S_TB_COMBINEBUTTONS     "Combine &buttons:"
#define S_TB_TRAYWND            "Notification area:"
#define S_TB_TRAYWND_CUSTOMIZE  "&Customize..."
#define S_TB_PEEK               "Use &Peek to preview the desktop when you move the mouse cursor to the Show desktop button at the end of the taskbar"

#define S_TB_MM                "Multiple displays"
#define S_TB_ALLDISPLAYS       "&Show taskbar on all displays"
#define S_TB_MMDISPLAYS        "S&how buttons on:"
#define S_TB_MMCOMBINEBUTTONS  "Combine buttons on &other taskbars:"

#define S_ADV  "Advanced Settings"

#define S_ADV_PERFORMANCE     "Performance"
#define S_ADV_ANIMATIONS      "Enable additional &animations"
#define S_ADV_SAVETHUMBNAILS  "Save window preview &thumbnails"

#define S_ADV_NAVIGATION      "Navigation"
#define S_ADV_WINXPOWERSHELL  "Replace Command Prompt with &PowerShell in the menu when right-clicking the start button or pressing Windows+X *"

#define S_ADV_MISC            "Miscellaneous"
#define S_ADV_SHOWDESKTOP     "Enable Show &Desktop button **"
#define S_ADV_TOGGLEAUTOHIDE  "Toggle &hiding the taskbar when double clicking it **"

#define S_ADV_RESTARTEXPLORER_T  "* Requires restarting Explorer after applying to take effect"
#define S_ADV_EXPLORERPATCHER_T  "** Requires ExplorerPatcher or similar software"
#define S_ADV_RESTARTEXPLORER    "<A ID=""restart"">Restart Explorer</A>"

#define S_TB_POS_L      "Left"
#define S_TB_POS_T      "Top"
#define S_TB_POS_R      "Right"
#define S_TB_POS_B      "Bottom"
#define S_TB_COMB_YES   "Always, hide labels"
#define S_TB_COMB_FULL  "When taskbar is full"
#define S_TB_COMB_NO    "Never"
#define S_TB_MMALL      "All taskbars"
#define S_TB_MMMAIN     "Main taskbar and taskbar where the window is open"
#define S_TB_MMCUR      "Taskbar where the window is open"

#include "template.rc"
#include "undef.h"
