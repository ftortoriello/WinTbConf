#undef RES_LANG
#undef RES_SUBLANG

#undef S_ERROR
#undef S_ERROR_GENERIC
#undef S_ERROR_MEM

#undef S_TB

#undef S_TB_TB
#undef S_TB_LOCK
#undef S_TB_AUTOHIDE
#undef S_TB_SMALLBUTTONS
#undef S_TB_BADGES
#undef S_TB_LOCATION
#undef S_TB_COMBINEBUTTONS
#undef S_TB_TRAYWND
#undef S_TB_TRAYWND_CUSTOMIZE
#undef S_TB_PEEK

#undef S_TB_MM
#undef S_TB_ALLDISPLAYS
#undef S_TB_MMDISPLAYS
#undef S_TB_MMCOMBINEBUTTONS

#undef S_ADV

#undef S_ADV_PERFORMANCE
#undef S_ADV_ANIMATIONS
#undef S_ADV_SAVETHUMBNAILS

#undef S_ADV_NAVIGATION
#undef S_ADV_WINXPOWERSHELL

#undef S_ADV_MISC
#undef S_ADV_SHOWDESKTOP
#undef S_ADV_TOGGLEAUTOHIDE

#undef S_ADV_RESTARTEXPLORER_T
#undef S_ADV_EXPLORERPATCHER_T
#undef S_ADV_RESTARTEXPLORER

#undef S_TB_POS_L
#undef S_TB_POS_T
#undef S_TB_POS_R
#undef S_TB_POS_B
#undef S_TB_COMB_YES
#undef S_TB_COMB_FULL
#undef S_TB_COMB_NO
#undef S_TB_MMALL
#undef S_TB_MMMAIN
#undef S_TB_MMCUR
