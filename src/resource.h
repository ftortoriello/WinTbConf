#pragma once
#if !defined(RESOURCE_H)
#define RESOURCE_H

#define VERSION      1,1,3,0
#define VERSION_STR  "1.1.3\0"

#define IDC_STATIC  -1

/* Dialogs */
#define IDD_TB    10
#define IDD_ADV  20

/* Controls */

/* General page */
#define IDC_TB_LOCK              100
#define IDC_TB_AUTOHIDE          101
#define IDC_TB_SMALLBUTTONS      102
#define IDC_TB_BADGES            103
#define IDC_TB_LOCATION          104
#define IDC_TB_COMBINEBUTTONS    105
#define IDC_TB_TRAYWND           106
#define IDC_TB_PEEK              107
#define IDC_TB_ALLDISPLAYS       108
#define IDC_TB_MMDISPLAYS        109
#define IDC_TB_MMCOMBINEBUTTONS  110

/* Advanced page */
#define IDC_ADV_ANIMATIONS      100
#define IDC_ADV_SAVETHUMBNAILS  101
#define IDC_ADV_WINXPOWERSHELL  102
#define IDC_ADV_SHOWDESKTOP     103
#define IDC_ADV_TOGGLEAUTOHIDE  104

/* Strings */

#define IDS_PROPSHEET_NAME  41
#define IDS_ERROR           60
#define IDS_ERROR_GENERIC   61
#define IDS_ERROR_MEM       62

/* Combobox list items */
#define IDS_TB_POS_L      200
#define IDS_TB_POS_T      201
#define IDS_TB_POS_R      202
#define IDS_TB_POS_B      203
#define IDS_TB_COMB_YES   204
#define IDS_TB_COMB_FULL  205
#define IDS_TB_COMB_NO    206
#define IDS_TB_MMALL      207
#define IDS_TB_MMMAIN     208
#define IDS_TB_MMCUR      209

#endif  /* !defined(RESOURCE_H) */
