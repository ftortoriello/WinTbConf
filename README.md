# Classic Windows taskbar configurator

Provides a taskbar settings dialog similar to Windows versions prior to 1607,
with some extra features.

It is designed for the Windows 10 taskbar only. Some settings are compatible
with newer versions, but others may do nothing or cause unexpected effects.

This software has _no_ external dependencies, not even the Visual C runtime.

## Building the program

* A standard Makefile and scripts are provided for building with MinGW.

* In addition, on the `vs17` folder there is a solution for Microsoft Visual
  Studio 2022.
